/*
Сложение чисел массива
*/
#include "pch.h"
#include <iostream>
#include <time.h>
#include <omp.h>
#include <chrono>

using namespace std;

int rand_num() {   //Функция для рандома числа
	return rand() % (999 - 0 + 1) + 0;
}

int summary_successively(int *a, int n, int sum) {
	for (int i = 0; i < n; i++) {
		sum += a[i];
	}
	return sum;
}

int summary_parallel(int *a, int n) {
	int sum = 0;

#pragma omp parallel shared(a) reduction (+: sum) num_threads(2)
	{
#pragma omp for
		for (int i = 0; i < n; i++)
			sum += a[i];
	}
	return sum;
}

int main(int argc, char* argv[])
{
	srand(static_cast<unsigned int>(time(NULL)));
	const int n = atoi(argv[1]); //Количество элементов
	cout << "Elements in array: " << n << endl;
	int *a = new int[n]; //Массив целых чисел
	for (int i = 0; i < n; i++) {  //Заполнение массива случайными числами
		a[i] = rand_num();
	}
	int sum_suc = 0;
	auto begin_suc = chrono::steady_clock::now();
	sum_suc = summary_successively(a, n, sum_suc);
	auto end_suc = chrono::steady_clock::now();
	auto elapsed_ms_suc = chrono::duration_cast<chrono::milliseconds>(end_suc - begin_suc);
	cout << "Time successively: " << elapsed_ms_suc.count() << " ms" << endl;

	int sum_par = 0;
	auto begin_par = chrono::steady_clock::now();
	sum_par = summary_parallel(a, n);
	auto end_par = chrono::steady_clock::now();
	auto elapsed_ms_par = chrono::duration_cast<chrono::milliseconds>(end_par - begin_par);
	cout << "Time parallel: " << elapsed_ms_par.count() << " ms";
}
